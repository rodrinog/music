Sistema para treino de percepção auditiva utilizando elementos musicais.

Iremos desenvolver um sistema que contenha as seguintes categorias de exercicios:
- Estudo de intervalo - entre duas notas;
- Estudo de acordes - tríades;
- Estudo de acordes - tetrades;
- Estudo de escalas - e possiveis encadeamentos;
- Estudo de melodia - ditado melódico;
- Estudo de ritmo   - ditado ritmico;

Em breve mais informações.
